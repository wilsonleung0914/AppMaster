import {Component, Input} from '@angular/core';

@Component({
    selector: 'alert-item',
    templateUrl: './alert-item.component.html',
    styleUrls: ['./alert-item.component.scss'],
})
export class AlertItemComponent {

    @Input('show')_show: boolean;
    _content: string;
    _bgColor: string;

    @Input()
    set content(value: string) {
        this._content = (value !== undefined && value !== null) ? value : '';
    }
    @Input()
    set bgColor(value: string) {
        this._bgColor = (value !== undefined && value !== null) ? value : '';
    }

    constructor() {
    }

}
