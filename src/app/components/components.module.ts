import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {ShellModule} from '../shell/shell.module';

import {AlertItemComponent} from './alert-item/alert-item.component';
import {RefresherComponent} from './refresher/refresher.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ShellModule,
        IonicModule.forRoot()
    ],
    declarations: [
        AlertItemComponent,
        RefresherComponent,
    ],
    exports: [
        ShellModule,
        AlertItemComponent,
        RefresherComponent,
    ]
})
export class ComponentsModule {
}
