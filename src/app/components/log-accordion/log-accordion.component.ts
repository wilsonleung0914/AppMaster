import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ServerLogModel} from '../../models/server-log.model';

@Component({
    selector: 'log-accordion',
    templateUrl: './log-accordion.component.html',
    styleUrls: ['./log-accordion.component.scss'],
})
export class LogAccordionComponent {

    @Input() _log : ServerLogModel;
    @Input()
    set log(value: ServerLogModel) {
        this._log = value;
    }
    @Input('expanded') expanded: boolean = false;
    @Output('toggle') expandClicked = new EventEmitter();
    @Output('go') buttonClicked     = new EventEmitter();

    constructor(
    ) {
        this._log = ServerLogModel.init({});
    }

    /* ==================================================================================================== */
    /* Manipulate View */

    toggleVisibility(field: string = '') {
        let visible = false;
        if (this.hasContent(field)) {
            visible = true;
        }
        return visible;
    }

    /* ==================================================================================================== */
    /* Operations */

    hasContent(field: string = '') {
        return (field !== null && field.length > 0);
    }

    expand() {
        this.expandClicked.emit();
        this.expanded = !this.expanded;
    }

    navigate() {
        this.buttonClicked.emit();
    }

}
