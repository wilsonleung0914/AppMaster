import { Injectable } from '@angular/core';
import {HTTP} from '@ionic-native/http/ngx';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AngularFirestore} from '@angular/fire/firestore';
import {LocalNotifications} from '@ionic-native/local-notifications/ngx';

import * as Constants from './constants';
import {DateService} from './dateService';
import {DataService} from './dataService';
import {Observable} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {LogService} from './apiServices/logService';

@Injectable({
  providedIn: 'root'
})
export class PushService {

    constructor(
        protected http            : HTTP,
        protected httpClient      : HttpClient,
        protected fireStore       : AngularFirestore,
        protected dateService     : DateService,
        protected dataService     : DataService,
        protected logService      : LogService,
        public localNotifications : LocalNotifications,
    ) {
    }

    getScheduleTime(minutes ?: number) : Date {
        const targetTime = new Date();
        targetTime.setHours(0);
        targetTime.setMinutes(minutes);
        return targetTime;
    }

    sendNotification() {
        const notificationOptions = {
            icon: 'https://zerex.net/app/wp-content/uploads/2021/05/devotion.jpg',
            text: `夠鐘！！！\nCheck App～～～`,
            group: 'dataValidation',
            data: { secret: 'secret' },
            foreground: true,
            launch: true,
        };

        this.localNotifications.schedule([
            { id: 3, ...notificationOptions, trigger: { at: this.getScheduleTime(3) } },
            { id: 6, ...notificationOptions, trigger: { at: this.getScheduleTime(6) } },
            { id: 9, ...notificationOptions, trigger: { at: this.getScheduleTime(9) } },
            // {
            //     id: 1,
            //     icon: 'https://zerex.net/app/wp-content/uploads/2021/05/devotion.jpg',
            //     text: `夠鐘！！！\n請檢查「每日靈修」。`,
            //     group: 'dataValidation',
            //     data: { secret: 'secret' },
            //     foreground: true,
            //     launch: true,
            //     trigger: { at: targetTime }
            // }
        ]);
    }


}
