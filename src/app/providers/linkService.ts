import {Injectable} from '@angular/core';
import {HTTP} from '@ionic-native/http/ngx';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import * as Constants from './constants';
import {distinctUntilChanged, retry, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'apiKey':  '87eee698f3dc4563b940397457489d60',
    })
};

@Injectable({
    providedIn: 'root'
})
export class LinkService {

    constructor(
        private http       : HTTP,
        private httpClient : HttpClient,
    ) {
    }

    getLink() : Observable<any> {
        let url = Constants.API_REBRANDLY + '40fbab69614742868acd58d4cc156f6a';
        return this.httpClient.get<any>(url, httpOptions)
            .pipe(
                tap(output => console.log('api output: ', output)),
                retry(2),
                distinctUntilChanged(),
            );
    }

    updateLink(item: any) : Observable<any> {
        let url = Constants.API_REBRANDLY + '40fbab69614742868acd58d4cc156f6a';
        let params = {
            title: item.title,
            destination: item.link,
            favourite: true,
        };
        return this.httpClient.post<any>(url, params, httpOptions);
    }

}
