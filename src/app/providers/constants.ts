/* ==================================================================================================== */
/* API URLS */

export const WKPHC_MAIN    = 'https://www.wkphc.org/';
export const API_MAIN      = 'https://www.wkphc.org/wp-json/wp/v2';
export const API_DEVOTION  = 'https://devotion.wkphc.org/wp-json/wp/v2';
export const API_PRAY      = 'https://pray.wkphc.org/wp-json/wp/v2';
export const API_OAUTH     = 'https://login.wkphc.org/oauth';
export const API_USER      = 'https://login.wkphc.org/wp-json/wp/v2/users';
export const API_MEMBER    = 'https://login.wkphc.org/wp-json/member/v1/route';
export const API_MEMCARD   = 'https://memcard2.wkphc.org/app';
export const API_REBRANDLY = 'https://api.rebrandly.com/v1/links/';
export const URL_RESET_PW  = 'https://www.wkphc.org/lostpassword/';

/* ==================================================================================================== */
/* FIREBASE_COLLECTIONS */

export const COLLECTION = {
    TESTING       : 'testing',
    POOL          : 'pool',
    ALERTS        : 'alerts',
    MEETINGS      : 'meetings',
    NOTIFICATIONS : 'notifications',
    DAILY_VERSE   : 'dailyVerse',
    SERVER_LOGS   : 'server-logs',
    SERVER_ERRORS : 'server-errors',

    STAGING : {
        ALERTS   : 'staging-alerts',
        MEETINGS : 'staging-meetings',
    },
};

/* ==================================================================================================== */
/* STORAGE KEYS */

export const KEY_APP_SETTINGS         = 'APP_SETTINGS';
export const KEY_SEEN_TUTORIAL        = 'SEEN_TUTORIAL';

/* ==================================================================================================== */
/* SETTINGS */

export const KEY_FONT_SIZE        = 'FONT_SIZE';
export const MAIN_FONT_SIZE       = 16;
export const TIMEOUT_SHORT        = 120000; // 300000
export const TIMEOUT_LONG         = 900000;
export const TIMEOUT_HALF_HOUR    = 1800000;
export const TIMEOUT_FULL_HOUR    = 3600000;
export const URL_SEPARATOR        = '/';
export const TIME_FORMAT_FULL     = 'HH:mm:ss';
export const DATE_FORMAT_SHORT    = 'M月D日';
export const DATE_FORMAT_TC       = 'YYYY年M月D日';
export const DATE_FORMAT_EN       = 'YYYY-MM-DD';
export const DATE_FORMAT_YM       = 'YYYY-MM';
export const DATE_FORMAT_MD       = 'MM-DD';
export const DATE_FORMAT_ID_LONG  = 'YYYYMMDDHHmmss';
export const HUMAN_TIME_FORMAT_EN = 'YYYY-MM-DD HH:mm';
export const HUMAN_TIME_FORMAT_TC = 'YYYY年MM月D日 HH:mm';
export const TIMESTAMP_FORMAT_EN  = 'YYYY-MM-DD HH:mm:ss';
export const TIMESTAMP_FORMAT_TC  = 'YYYY年MM月D日 HH:mm:ss';
