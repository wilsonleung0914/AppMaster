import {Injectable} from '@angular/core';
import {Plugins} from '@capacitor/core';
import {Router} from '@angular/router';
/* ==================================================================================================== */
/* Services */
import {DateService} from './dateService';
import {DataService} from './dataService';
import {FirebaseService} from './firebaseService';
import {LogService} from './apiServices/logService';
import {PushService} from './pushService';
import {MessageService} from './messageService';
/* ==================================================================================================== */
/* Data Related */
import * as Constants from './constants';
import {PageModel} from '../models/page.model';
import {AlertModel} from '../models/alert.model';
import {NotificationModel} from '../models/notification.model';
import {VerseModel} from '../models/verse.model';

const {Storage,Device} = Plugins;


@Injectable({
    providedIn: 'root'
})
export class GlobalVars {

    /* ==================================================================================================== */
    /* Setting Related */
    public todayDate      : string;
    public todayTimestamp : string;
    /* ==================================================================================================== */
    /* Data Related */
    public appPages               : PageModel[];
    public alertCollection        : AlertModel[];
    public notificationCollection : NotificationModel[];
    public verseCollection        : VerseModel[];
    public currentNotification    : NotificationModel;
    public currentVerse           : VerseModel;

    constructor(
        private router         : Router,
        public dateService     : DateService,
        public dataService     : DataService,
        public firebaseService : FirebaseService,
        public logService      : LogService,
        public pushService     : PushService,
        public messageService  : MessageService
    ) {
        this.todayDate = this.dateService.getCurrentDate();
        this.appPages  = [
            {id: 0, parent_id: -1, order: 0, show: true, title: '緊急訊息', icon: './assets/icons/menu/ic_alert.svg', url: '/alerts', children: []},
            {id: 1, parent_id: -1, order: 1, show: true, title: '我的通知', icon: './assets/icons/menu/ic_announcement.svg', url: '/notifications', children: []},
            {id: 2, parent_id: -1, order: 2, show: true, title: '每日聖言', icon: './assets/icons/menu/ic_bible.svg', url: '/verses', children: []},
            {id: 3, parent_id: -1, order: 3, show: true, title: 'Staging Pool', icon: './assets/icons/menu/ic_pool.svg', url: '/pool', children: []},
            // {id: 4, parent_id: -1, order: 4, show: true, title: 'API Monitor', icon: './assets/icons/menu/ic_monitor.svg', url: '/monitors', children: []},
        ];
        this.alertCollection        = [];
        this.notificationCollection = [];
        this.verseCollection        = [];
        this.currentVerse           = VerseModel.init(null);
        this.currentNotification    = NotificationModel.init(null);
    }

    /* ==================================================================================================== */
    /* Setting Related */

    isEmpty(content) {
        return content === null || content.length === 0;
    }

    linkify(inputText) : string {
        let replacedText;

        //URLs starting with http://, https://, or ftp://
        const replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\\/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|])/gim;
        replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

        //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
        const replacePattern2 = /(^|[^/])(www\.[\S]+(\b|$))/gim;
        replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

        //Change email addresses to mailto:: links.
        const replacePattern3 = /(([a-zA-Z0-9\-_.])+@[a-zA-Z_]+?(\.[a-zA-Z]{2,6})+)/gim;
        replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

        return replacedText;
    }

    updateDate() {
        this.todayDate      = this.dateService.getCurrentDate();
        this.todayTimestamp = this.dateService.getCurrentTime();
    }

    // async onDevice() {
    //     const info = Device.getInfo();
    //     info.then(results => {
    //         return (results.platform === 'ios' || results.platform === 'android');
    //     });
    // }

    /* ==================================================================================================== */
    /* Data Related */

    async setItem(key, val) {
        await Storage.set({key, value: JSON.stringify(val)});
    }

    async getItem(key) {
        const ret = await Storage.get({ key });
        return JSON.parse(ret.value);
    }

    async removeItem(key) {
        await Storage.remove({key});
    }

    /* ==================================================================================================== */

    /* PAGES & ROUTES */

    async navigatePage(url: string) {
        await this.goToPageByURL(url, {});
    }

    goToPageByURL(url: string, params: any) {
        this.router.navigate([url, params])
            .then(() => {});
    }

    /* ==================================================================================================== */

    /* APIs */

    async fetchData() {
        this.dataService.alert$          = this.firebaseService.getAlerts();
        this.dataService.notification$   = this.firebaseService.getNotifications();
        // this.dataService.notification$ = this.firebaseService.getSpecificNotifications();
        this.dataService.verse$          = this.firebaseService.getVerses();
        // this.dataService.logs$           = this.logService.getServerLogs();
        // this.dataService.errors$         = this.logService.getServerErrors();
        this.dataService.stagingAlerts$  = this.firebaseService.getStagingAlerts();
        this.dataService.stagingMeeting$ = this.firebaseService.getStagingMeetings();

        this.dataService.notification$
            .subscribe(items => this.notificationCollection = items);

        // this.firebaseService.getSpecificVerses()
        //     .subscribe(items => {
        //         // console.log('items: ', items);
        //         items.forEach(item => this.firebaseService.deleteDocuments(Constants.COLLECTION.DAILY_VERSE, item.doc_id));
        //     });

        // this.logService.getOldLogs()
        //     .subscribe(values => {
        //         // console.log('values: ', values);
        //     });

        // await this.dataService.logs$
        //     .subscribe(values => {
        //         // values.forEach(value => {
        //         //     for (let item of value['values']) {
        //         //         if (item.log_date !== this.dateService.getTodayDate()) {
        //         //             console.log('%s - item: ', value['key'], item.log_date);
        //         //         }
        //         //     }
        //         // });
        //         // values.forEach(value => console.log(`key: ${value['key']},\nlength: ${value['values'].length},\nvalue: `, value['values']));
        //         // console.log('values: ', values);
        //     });

        // await this.logService.getServerErrors()
        //     .subscribe(values => {
        //         const info = Device.getInfo();
        //         info.then(results => {
        //             if (results.platform === 'ios' || results.platform === 'android') {
        //                 // this.pushService.sendNotification();
        //             } else {
        //                 // console.log('values: ', values);
        //             }
        //         });
        //     });
    }

    // public async fetchAlertCollection() {
    //     await this.firebaseService.getAlertDataSource()
    //         .subscribe(response => {
    //             this.alertCollection = response as AlertModel[];
    //             this.setItem(Constants.COLLECTION.ALERTS, this.alertCollection);
    //         });
    // }
    //
    // public async fetchNotificationCollection() {
    //     await this.firebaseService.getNotificationDataSource()
    //         .subscribe(response => {
    //             this.notificationCollection = response as NotificationModel[];
    //             // console.log('notificationCollection: ', this.notificationCollection);
    //         });
    // }
    //
    // public async fetchDailyVerseCollection() {
    //     await this.firebaseService.getDailyVerseDataSource()
    //         .subscribe(response => {
    //             this.verseCollection = response.filter(item => item.date.includes(this.dateService.getCurrentMoment().format(Constants.DATE_FORMAT_YM)));
    //             // console.log('verseCollection: ', this.verseCollection);
    //         });
    // }
}
