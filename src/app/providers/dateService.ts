import {Injectable} from '@angular/core';
import * as moment from 'moment';
import * as Constants from './constants';

@Injectable({
    providedIn: 'root'
})
export class DateService {

    constructor(
    ) {
    }

    // ! dates are in ISO 8601 format,
    // ! HKT = 2020-01-01T00:00:00.000Z
    // ! ISO = 2020-01-01T00:00:00.000+08:00

    /* ==================================================================================================== */

    /* MOMENT */
    momentDate(date : string) : moment.Moment {
        return moment(date).utcOffset('+08:00');
    }

    getCurrentMoment() : moment.Moment {
        return moment();
    }

    /* TIMESTAMP */
    getTodayTimestamp() : moment.Moment {
        return moment().startOf('day');
    }
    getTodayTimestampISO() : string {
        return moment().utcOffset('+08:00').startOf('day').toISOString(true);
    }

    getCurrentTimestamp() : moment.Moment {
        return moment().utcOffset('+08:00');
    }

    getCurrentISOString() : string {
        return this.getCurrentTimestamp().toISOString(true);
    }

    /* ==================================================================================================== */

    /* STRING */
    formatDate(date : string, format : string = Constants.TIMESTAMP_FORMAT_EN) : string {
        return this.momentDate(date).format(format);
    }

    formatDateString(date : string, format : string = Constants.HUMAN_TIME_FORMAT_TC, prefix : string = '發佈時間：', suffix : string = '') : string {
        return prefix + this.momentDate(date).format(format) + suffix;
    }

    getTodayDate() : string {
        return this.getCurrentTimestamp().format(Constants.DATE_FORMAT_EN);
    }

    getCurrentDate() : string {
        return this.getCurrentTimestamp().format(Constants.DATE_FORMAT_EN);
    }

    getCurrentTime() : string {
        return this.getCurrentTimestamp().format(Constants.TIMESTAMP_FORMAT_EN);
    }

    getDateBefore(dayNumber : number) : string {
        return this.getCurrentTimestamp().subtract(dayNumber, 'days').format(Constants.DATE_FORMAT_EN);
    }

    getDateAfter(dayNumber : number) : string {
        return this.getCurrentTimestamp().add(dayNumber, 'days').format(Constants.DATE_FORMAT_EN);
    }

    /* ==================================================================================================== */

    /* Number */

    getCurrentTimeNumber() : number {
        return Number(this.getCurrentTimestamp().format(Constants.DATE_FORMAT_ID_LONG));
    }

    /* ==================================================================================================== */

    /* COMPARE */
    validDate(date1 : string, date2 : string) : boolean {
        return this.getCurrentTimestamp().isBetween(date1, date2, null, '[]');
    }

    inRange(lowerBound : string, upperBound : string) : boolean {
        return this.getCurrentTimestamp().isBetween(lowerBound, upperBound, null, '[]');
    }

    isSame(targetDate : string, granularity : any = 'day') : boolean {
        return this.getCurrentTimestamp().isSame(targetDate, granularity);
    }
    isBefore(targetDate : string, granularity : any = 'day') : boolean {
        return this.getCurrentTimestamp().isBefore(targetDate, granularity);
    }
    isAfter(targetDate : string, granularity : any = 'day') : boolean {
        return this.getCurrentTimestamp().isAfter(targetDate, granularity);
    }

    checkSafeDate(date : string) : boolean {
        return (date === this.getTodayDate());
        // return (date === this.getTodayDate() || date === this.getDateBefore(1));
    }

}
