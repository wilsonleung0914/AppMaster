import {Injectable} from '@angular/core';
import {
    AlertController,
    ToastController
} from '@ionic/angular';
// import {SocialSharePage} from '../pages/social-share/social-share';
// import {ListPopoverPage} from '../pages/popovers/list-popover/list-popover';

@Injectable({
    providedIn: 'root',
})
export class MessageService {


    constructor(
        public alertCtrl       : AlertController,
        public toastCtrl       : ToastController,
    ) {
    }

    /* ==================================================================================================== */
    /* Alert */

    async getTopAlert() {
        return await this.alertCtrl.getTop()
    }

    async presentGeneralAlert(header, subHeader, message, buttons: any = [], backdropDismiss: boolean = false, cssClass: string = '') {
        // handle alert buttons
        if (!buttons.length) {
            buttons.push('好的');
        }

        // handle alert options
        let alertOptions: any = {
            header: header,
            subHeader: subHeader,
            message: message,
            buttons: buttons,
            backdropDismiss: backdropDismiss,
            cssClass: (buttons.length == 2) ? 'alert-confirmation' : cssClass,
        };

        let alert = await this.alertCtrl.create(alertOptions);
        return await alert.present();
    }

    async presentConfirmationAlert(header, subHeader, message, positiveHandler) {
        // handle alert buttons
        let buttons = [
            {
                text: '取消',
                role: 'cancel',
                handler: () => {},
            },
            {
                text: '確認',
                cssClass: 'danger',
                handler: positiveHandler,
            }
        ];

        // handle alert options
        let alertOptions: any = {
            header: header,
            subHeader: subHeader,
            message: message,
            buttons: buttons,
            cssClass: 'alert-confirmation',
        };

        let alert = await this.alertCtrl.create(alertOptions);
        return await alert.present();
    }

    /* ==================================================================================================== */
    /* Toast */

    async presentToast(message) {
        const toast = await this.toastCtrl.create({
            message,
            duration: 5000,
        });
        return await toast.present();
    }

}
