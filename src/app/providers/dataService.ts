import {Injectable} from '@angular/core';
import {Plugins} from "@capacitor/core";
import {Observable} from 'rxjs';

import {AlertModel} from '../models/alert.model';
import {NotificationModel} from '../models/notification.model';
import {VerseModel} from '../models/verse.model';
import {ServerLogModel} from '../models/server-log.model';
import {MeetingModel} from '../models/meeting.model';

const { Storage } = Plugins;

@Injectable({
    providedIn: 'root'
})
export class DataService {

    // ---------- Firestore Data ---------- //
    alert$          : Observable<AlertModel[]>;
    notification$   : Observable<NotificationModel[]>;
    verse$          : Observable<VerseModel[]>;
    logs$           : Observable<ServerLogModel[]>;
    errors$         : Observable<ServerLogModel[]>;
    stagingAlerts$  : Observable<AlertModel[]>;
    stagingMeeting$ : Observable<MeetingModel[]>;

    constructor() {
    }

    async set(key: string, value: any): Promise<void> {
        await Storage.set({
            key: key,
            value: JSON.stringify(value)
        });
    }

    async get(key: string): Promise<any> {
        const item = await Storage.get({key: key});
        return JSON.parse(item.value);
    }

    async remove(key: string): Promise<void> {
        await Storage.remove({
            key: key
        });
    }


}
