import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable, of, zip} from 'rxjs';
import {distinctUntilChanged, filter, map, take, tap} from 'rxjs/operators';
/* ==================================================================================================== */
/* Models */
import {AlertModel} from '../models/alert.model';
import {MeetingModel} from '../models/meeting.model';
import {NotificationModel} from '../models/notification.model';
import {VerseModel} from '../models/verse.model';

import * as Constants from './constants';
import {DateService} from './dateService';
import {LogService} from './apiServices/logService';
import {LinkService} from './linkService';

// import {FirebaseX} from '@ionic-native/firebase-x/ngx';

@Injectable({
    providedIn: 'root'
})
export class FirebaseService {

    constructor(
        // private firebaseX: FirebaseX,
        private fireStore  : AngularFirestore,
        public logService : LogService,
        public dateService : DateService,
        public linkService : LinkService,
    ) {
    }

    // /* ================================================== */ //
    /* General */

    deleteDocuments(collection_key, doc_id) {
        this.fireStore
            .collection(collection_key)
            .doc(String(doc_id))
            .delete()
            .then(() => console.log(`delete ${doc_id} from ${collection_key}.`));
    }

    // /* ================================================== */ //
    /* Alert */

    getAlerts() : Observable<AlertModel[]> {
        return this.fireStore
            .collection<AlertModel>(Constants.COLLECTION.ALERTS, ref => ref.orderBy('id', 'asc'))
            .snapshotChanges().pipe(
                map(items => items.map(value => AlertModel.init(value.payload.doc.data()))),
                distinctUntilChanged(),
            );
    }

    // /* ================================================== */ //
    /* Notification */

    getNotifications() : Observable<NotificationModel[]> {
        return this.fireStore
            // .collection<NotificationModel>(Constants.COLLECTION.NOTIFICATIONS, ref => ref.orderBy('id', 'asc'))
            .collection<NotificationModel>(Constants.COLLECTION.NOTIFICATIONS, ref => ref.orderBy('id', 'desc'))
            .snapshotChanges().pipe(
                map(items => items
                    .map(value => {
                        const notification = NotificationModel.init(value.payload.doc.data());
                        notification.doc_id = value.payload.doc.id;
                        return notification;
                    })
                    // .filter(item => item.expired_time < '2020-11-01 00:00:00')
                ),
                // tap(output => console.log('api output: ', output)),
                // tap(output => output.map(item => this.updateNotification(item))),
                distinctUntilChanged(),
            );
    }
    getSpecificNotifications() : Observable<any[]> {
        return this.fireStore
            .collection<any>(Constants.COLLECTION.NOTIFICATIONS, ref => ref.orderBy('id', 'desc'))
            .snapshotChanges().pipe(
                map(items => items
                    .map(value => {
                        const notification = NotificationModel.init(value.payload.doc.data());
                        notification.doc_id = value.payload.doc.id;
                        return notification;
                    })
                    // .filter(item => item.doc_id === 'tc4tPzL4R05xXwbcVGkZ')
                    // .filter(item => item.expired_time <= '2020-12-31 23:59:59')
                    // .filter(item => (item.release_time >= '2021-07-01 00:00:00'))
                    // .filter(item => (item.release_time < '2020-10-01 00:00:00') && (item.release_time > '2020-02-05 00:00:00'))
                    // .filter(item => item.title.includes('疫情速遞'))
                ),
                // tap(output => console.log('api output: ', output.map(item => item.title))),
                // tap(output => output.map(item => this.updateNotification(item))),
                take(1),
                distinctUntilChanged(),
            );
    }

    removeOldNotification(notification_id) {
        // console.log('noti id: ', notification_id);
        this.fireStore
            .collection(Constants.COLLECTION.NOTIFICATIONS)
            .doc(String(notification_id))
            .delete()
            .then(() => console.log('deleted key: ', notification_id));
    }
    hideOldNotification(notification_id) {
        // console.log('noti id: ', notification_id);
        this.fireStore
            .collection(Constants.COLLECTION.NOTIFICATIONS)
            .doc(String(notification_id))
            .set({
                show: false,
            }, {merge: true});
    }
    catNotification(notification_id) {
        this.fireStore
            .collection(Constants.COLLECTION.NOTIFICATIONS)
            .doc(String(notification_id))
            .set({
                level: 1,
            }, {merge: true});
    }

    public async updateNotification(model : any) {
        // let target = (model as NotificationModel).toObject();
        // // target.expired_time = '2021-12-31 23:59:59';
        // // target.show = true;
        // await this.fireStore
        //     .collection(Constants.COLLECTION.NOTIFICATIONS)
        //     .doc(model.doc_id)
        //     .set(target, {merge: true})
        //     .then(() => {
        //         // console.log('updated model: ', target);
        //     });
    }

    // // /* ================================================== */ //
    // /* Notification */
    // public getNotificationDataSource(): Observable<Array<NotificationModel>> {
    //     return this.afs
    //         .collection<any>(Constants.COLLECTION.NOTIFICATIONS, ref => ref.orderBy('id', 'desc'))
    //         .snapshotChanges().pipe(
    //             map(values => {
    //                 return values.map(value => {
    //                     return value.payload.doc.data();
    //                 });
    //             })
    //         );
    // }
    // public getNotificationStore(dataSource: Observable<Array<NotificationModel>>): DataStore<Array<NotificationModel>> {
    //     // Use cache if available
    //     if (!this.notificationDataStore) {
    //         // Initialize the model specifying that it is a shell model
    //         const shellModel: Array<NotificationModel> = [
    //             new NotificationModel(),
    //             new NotificationModel(),
    //         ];
    //
    //         this.notificationDataStore = new DataStore(shellModel);
    //         // Trigger the loading mechanism (with shell) in the dataStore
    //         this.notificationDataStore.load(dataSource);
    //     }
    //     return this.notificationDataStore;
    // }
    // // public getNotificationDetail(id: string): Observable<NotificationModel> {
    // //     return this.getNotificationDataSource()
    // //         .pipe(
    // //             map(values => {
    // //                 return values.map(value => {
    // //                     return value.payload.doc.data();
    // //                 });
    // //             })
    // //         );
    // // }
    //

    // /* ================================================== */ //
    /* Verse */

    getVerses() : Observable<VerseModel[]> {
        return this.fireStore
            // .collection<VerseModel>(Constants.COLLECTION.TESTING, ref => ref.orderBy('id', 'desc'))
            .collection<VerseModel>(Constants.COLLECTION.DAILY_VERSE, ref => ref.orderBy('id', 'desc'))
            .snapshotChanges().pipe(
                map(items => items
                    .map(value => {
                        const verse = VerseModel.init(value.payload.doc.data());
                        verse.doc_id = value.payload.doc.id;
                        return verse;
                    })
                    // .filter(item => item.expired_time.includes('06:29:59'))
                    // .filter(item => (item.release_time >= '2021-07-01 00:00:00'))
                    // .filter(item => item.date.includes(this.dateService.getCurrentMoment().format(Constants.DATE_FORMAT_YM)))
                ),
                // tap(output => console.log('api output: ', output.map(item => item.date))),
                // tap(output => output.map(item => this.updateDailyVerse(item))),
                distinctUntilChanged(),
            );
    }

    public async updateDailyVerse(model : any) {
        const target = (model as VerseModel).toObject();
        // target.expired_time = target.expired_time.replace('06:29:59','05:59:59');
        // console.log('target: ', target.expired_time);
        await this.fireStore
            .collection(Constants.COLLECTION.DAILY_VERSE)
            // .collection(Constants.COLLECTION.TESTING)
            .doc(model.doc_id)
            .set(target, {merge: true});
    }

    getSpecificVerses() : Observable<VerseModel[]> {
        return this.fireStore
            .collection<VerseModel>(Constants.COLLECTION.DAILY_VERSE, ref => ref.orderBy('id', 'asc'))
            .snapshotChanges().pipe(
                map(items => items
                    .map(value => {
                        const verse = VerseModel.init(value.payload.doc.data());
                        verse.doc_id = value.payload.doc.id;
                        return verse;
                    })
                    .filter(item => item.id < 701)
                ),
                // tap(output => console.log('api output: ', output)),
            );
    }

    // /* ================================================== */ //
    /* Pool */

    getStagingAlerts() : Observable<AlertModel[]> {
        return this.fireStore
            .collection<AlertModel>(Constants.COLLECTION.STAGING.ALERTS, ref => ref.orderBy('id', 'asc'))
            .snapshotChanges().pipe(
                // map(items => items.map(value => AlertModel.init(value.payload.doc.data()))),
                map(items => items.map(value => {
                    const alert = AlertModel.init(value.payload.doc.data());
                    alert.doc_id = value.payload.doc.id;
                    return alert;
                })),
                // tap(output => console.log('api output: ', output)),
                distinctUntilChanged(),
            );
    }

    getStagingMeetings() : Observable<MeetingModel[]> {
        return this.fireStore
            .collection<MeetingModel>(Constants.COLLECTION.STAGING.MEETINGS, ref => ref.orderBy('id', 'desc'))
            .snapshotChanges().pipe(
                map(items => items.map(value => {
                    const meeting = MeetingModel.init(value.payload.doc.data());
                    meeting.doc_id = value.payload.doc.id;
                    return meeting;
                })),
                // tap(output => console.log('meeting output: ', output)),
                distinctUntilChanged(),
            );
    }

    public async runUpdate(model : any) {
        let target; let key;
        if (model instanceof AlertModel) {
            target = (model as AlertModel).toObject();
            key    = Constants.COLLECTION.ALERTS;
        } else if (model instanceof MeetingModel) {
            target = (model as MeetingModel).toObject();
            key    = Constants.COLLECTION.MEETINGS;
        }
        await this.fireStore
            .collection(key)
            .doc(model.doc_id)
            .set(target, {merge: true});
    }

}
