import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {distinctUntilChanged, filter, groupBy, map, take, tap} from 'rxjs/operators';

import * as firebase from 'firebase';
import * as Constants from '../constants';
import {ServerLogModel} from '../../models/server-log.model';
import {DateService} from '../dateService';

/* ==================================================================================================== */
/* Models */

@Injectable({
    providedIn: 'root',
})
export class LogService {

    deviceID   : string ;
    appVersion : string ;

    constructor(
        // private device: Device,
        // private http: HTTP,
        private httpClient: HttpClient,
        private fireStore: AngularFirestore,
        public dateService: DateService,
        // public dataService: DataService,
    ) {
    }

    // getServerLogs() : Observable<any> {
    //     return this.fireStore
    //         .collection<any>(Constants.COLLECTION.SERVER_LOGS)
    //         .snapshotChanges()
    //         .pipe(
    //             map(items => items
    //                 // .filter(item => item.payload.doc.id.indexOf('error') == -1)
    //                 // .filter(item => item.payload.doc.id.indexOf('vh-log') > -1)
    //                 .map(value => {
    //                     let data   = value.payload.doc.data();
    //                     let result = [];
    //                     for (let key in data) {
    //                         data[key].forEach(item => result.push(ServerLogModel.init(item)));
    //                     }
    //                     return {
    //                         key: value.payload.doc.id,
    //                         values: result,
    //                     };
    //                 })
    //             ),
    //             distinctUntilChanged(),
    //         );
    // }
    //
    // getServerErrors() : Observable<any> {
    //     return this.fireStore
    //         .collection<any>(Constants.COLLECTION.SERVER_LOGS)
    //         .snapshotChanges()
    //         .pipe(
    //             map(items => items
    //                 .filter(item => item.payload.doc.id.indexOf('error') > -1)
    //                 .map(value => {
    //                     let data = value.payload.doc.data();
    //                     let result;
    //                     for (let key in data) {
    //                         result = data[key].map(value => ServerLogModel.init(value));
    //                     }
    //                     return {
    //                         key: value.payload.doc.id,
    //                         values: result,
    //                     };
    //                 })
    //             ),
    //             // tap(output => console.log('api output: ', output)),
    //             distinctUntilChanged(),
    //         );
    // }

    /* ==================================================================================================== */

    // ! Old Logs ! //

    // getOldLogs() : Observable<any> {
    //     return this.fireStore
    //         .collection(Constants.COLLECTION.SERVER_LOGS)
    //         .get()
    //         .pipe(
    //             map(res => res.docs
    //                 .filter(doc => doc.id.indexOf('error') == -1)
    //                 .map(doc => {
    //                     let data = doc.data();
    //                     let result = [];
    //                     for (let key in data) {
    //                         if (this.dateService.checkSafeDate(key)) continue;
    //                         result.push(key);
    //                     }
    //                     return {
    //                         key: doc.id,
    //                         values: result,
    //                     };
    //                 })
    //             ),
    //             tap(output => {
    //                 console.log('api output: ', output)
    //                 for (let i = 0; i < 7; i++) {
    //                     setTimeout(() => {
    //                         for (let item in output) {
    //                             this.clearOldDates({
    //                                 id: output[item].key,
    //                                 date: output[item].values,
    //                             });
    //                         }
    //                     }, 3000);
    //                 }
    //             }),
    //             take(1),
    //         );
    // }
    //
    // clearOldDates(args: any) {
    //     // console.log('args: ', args);
    //     console.log('remove field: ', args.id, args.date);
    //     args.date.forEach(date => {
    //         this.fireStore
    //             .collection(Constants.COLLECTION.SERVER_LOGS)
    //             .doc(args.id)
    //             .update({
    //                 [date]: firebase.firestore.FieldValue.delete()
    //             })
    //             .then(() => {});
    //     });
    // }

}
