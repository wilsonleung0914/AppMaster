import {Component, OnInit} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@capacitor/splash-screen';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {GlobalVars} from './providers/globalVars';
import {PushService} from './providers/pushService';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
    public selectedIndex = 2;

    constructor(
        private platform   : Platform,
        private statusBar  : StatusBar,
        public globalVars  : GlobalVars,
        public pushService : PushService,
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            SplashScreen.hide().then(() => {});

            this.pushService.sendNotification();
        });
    }

    async ngOnInit() {
        // // ---------- Fetch App Data ---------- //
        await this.globalVars.fetchData();

        // this.globalVars.firebaseService
        //     .getSpecificNotifications()
        //     .subscribe(values => {
        //         values.forEach(item => {
        //             // console.log('targets: ', item.title);
        // // //             // console.log('targets: ', item.expired_time);
        // // //             this.globalVars.firebaseService
        // // //                 .hideOldNotification(item.doc_id);
        // // // //         //         // .removeOldNotification(item.doc_id);
        // // //                 .catNotification(item.doc_id);
        //         });
        //     });

        // this.globalVars.firebaseService
        //     .fetchDeviceToken()
        //     .subscribe(values => {
        //         console.log('device tokens: ', values);
        //     });

        // this.globalVars.firebaseService
        //     .fetchArchives()
        //     .subscribe(values => {
        //         console.log('archives: ', values);
        //     });
        // this.globalVars.firebaseService
        //     .fetchBackup()
        //     .subscribe(values => {
        //         console.log('backup: ', values);
        //     });

        // this.globalVars.firebaseService
        //     .getVerses()
        //     .subscribe();
    }
}
