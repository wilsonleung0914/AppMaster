/* ==================================================================================================== */
/* Framework Basics */
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
/* ==================================================================================================== */
/* Modules */
import {HttpClientModule} from '@angular/common/http';
import {ComponentsModule} from './components/components.module';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {NgPipesModule} from 'ngx-pipes';
/* ==================================================================================================== */
/* Plugins */
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {HTTP} from '@ionic-native/http/ngx';
import {LocalNotifications} from '@ionic-native/local-notifications/ngx';
/* ==================================================================================================== */
/* Services */
/* ==================================================================================================== */
/* Custom */
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {environment} from '../environments/environment.prod';
import {NotificationFormModalModule} from './pages/notifications/notification-form/notification-form.module';
import {DailyVerseFormModalModule} from './pages/daily-verse/daily-verse-form/daily-verse-form.module';


@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot({
            rippleEffect: false,
            backButtonText: '返回',
            mode: 'ios',
        }),
        AppRoutingModule,
        HttpClientModule,
        ComponentsModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFirestoreModule, // firestore
        NotificationFormModalModule,
        DailyVerseFormModalModule,
        NgPipesModule,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        HTTP,
        LocalNotifications,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {
}
