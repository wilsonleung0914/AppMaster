import * as moment from 'moment';
import * as Constants from '../providers/constants';

export class ServerLogModel {

    log_key      : string ;
    log_type     : string ;
    api_type     : string ;
    status       : string ;
    timestamp    : string ;
    url          : string ;
    device_id    : string ;
    device_model : string ;
    os_version   : string ;
    app_version  : string ;
    log_date     : string ;

    constructor(log_key?: string, log_type?: string, api_type?: string, status?: string, timestamp?: string, url?: string, device_id?: string, device_model?: string, os_version?: string, app_version?: string, log_date?: string) {
        this.log_key      = log_key      ;
        this.log_type     = log_type     ;
        this.api_type     = api_type     ;
        this.status       = status       ;
        this.timestamp    = timestamp    ;
        this.url          = url          ;
        this.device_id    = device_id    ;
        this.device_model = device_model ;
        this.os_version   = os_version   ;
        this.app_version  = app_version  ;
        this.log_date     = log_date     ;
    }

    static init(value: any) {
        let instance = new ServerLogModel();
        instance.log_key      = value.log_key         || '' ;
        instance.log_type     = value.log_type        || '' ;
        instance.api_type     = value.api_type        || '' ;
        instance.status       = value.status          || '' ;
        instance.timestamp    = value.timestamp       || '' ;
        instance.url          = value.url             || '' ;
        instance.device_id    = value.device_id       || '' ;
        instance.device_model = value.device_model    || '' ;
        instance.os_version   = value.os_version      || '' ;
        instance.app_version  = value.app_version     || '' ;
        instance.log_date     = instance.getLogDate(value.timestamp);
        return instance;
    }

    getLogDate(timestamp) {
        return moment(timestamp).utcOffset('+08:00').format(Constants.DATE_FORMAT_EN);
    }

    toLog() {
        return {
            log_key      : this.log_key      ,
            log_type     : this.log_type     ,
            api_type     : this.api_type     ,
            status       : this.status       ,
            timestamp    : this.timestamp    ,
            url          : this.url          ,
            device_id    : this.device_id    ,
            device_model : this.device_model ,
            os_version   : this.os_version   ,
            app_version  : this.app_version  ,
        };
    }

}
