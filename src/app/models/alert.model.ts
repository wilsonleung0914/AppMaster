export class AlertModel {
    doc_id       : string;
    id           : number;
    position     : string;
    category     : string;
    content      : string;
    color        : string;
    release_time : string;
    expired_time : string;
    show         : boolean;

    constructor(doc_id?: string, id?: number, position?: string, category?: string, content?: string, color?: string, release_time?: string, expired_time?: string, show?: boolean) {
        this.doc_id       = doc_id;
        this.id           = id;
        this.position     = position;
        this.category     = category;
        this.content      = content;
        this.color        = color;
        this.release_time = release_time;
        this.expired_time = expired_time;
        this.show         = show;
    }

    static init(params: any) {
        let instance = new AlertModel();
        if (params === null) {
            return new AlertModel('', -1, '', '', '', '', '', '', false);
        }
        instance.doc_id       = params.id || '';
        instance.id           = params.id || -1;
        instance.id           = (params.id > 0) ? params.id : (params.id === 0) ? 0 : -1;
        instance.position     = params.position || '' ;
        instance.category     = params.category || '' ;
        instance.content      = params.content || '' ;
        instance.color        = params.color || '' ;
        instance.release_time = params.release_time || '' ;
        instance.expired_time = params.expired_time || '' ;
        instance.show         = params.show || false;
        return instance;
    }

    toObject() {
        return {
            id           : this.id           ,
            position     : this.position     ,
            category     : this.category     ,
            content      : this.content      ,
            color        : this.color        ,
            release_time : this.release_time ,
            expired_time : this.expired_time ,
            show         : this.show         ,
        };
    }
}
