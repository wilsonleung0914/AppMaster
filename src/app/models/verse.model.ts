export class VerseModel {
    doc_id       : string;
    id           : number;
    content      : string;
    verse        : string;
    date         : string;
    release_time : string;
    expired_time : string;

    constructor(doc_id?: string, id?: number, content?: string, verse?: string, date?: string, release_time?: string, expired_time?: string) {
        this.doc_id       = doc_id;
        this.id           = id;
        this.content      = content;
        this.verse        = verse;
        this.date         = date;
        this.release_time = release_time;
        this.expired_time = expired_time;
    }

    static init(value: any) {
        let instance = new VerseModel();
        if (value === null) return instance;
        instance.doc_id       = value.doc_id || '' ;
        instance.id           = value.id     || -1     ;
        instance.content      = value.content || '' ;
        instance.verse        = value.verse || '' ;
        instance.date         = value.date || '' ;
        instance.release_time = value.release_time || '' ;
        instance.expired_time = value.expired_time || '' ;
        return instance;
    }

    toObject() {
        return {
            id           : this.id           ,
            content      : this.content      ,
            verse        : this.verse        ,
            date         : this.date         ,
            release_time : this.release_time ,
            expired_time : this.expired_time ,
        };
    }
}
