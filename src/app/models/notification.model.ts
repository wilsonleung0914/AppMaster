export class NotificationModel {
    doc_id       : string;
    id           : number;
    topic        : string;
    label        : string;
    title        : string;
    excerpt      : string;
    content      : string;
    src          : string;
    bg           : string;
    link         : string;
    show         : boolean;
    read         : boolean;
    release_time : string;
    expired_time : string;

    constructor(id?: number, topic?: string, label?: string, title?: string, excerpt?: string, content?: string, src?: string, bg?: string, link?: string, show?: boolean, read?: boolean, release_time?: string, expired_time?: string) {
        this.doc_id       = '';
        this.id           = id;
        this.topic        = topic;
        this.label        = label;
        this.title        = title;
        this.excerpt      = excerpt;
        this.content      = content;
        this.src          = src;
        this.bg           = bg;
        this.link         = link;
        this.show         = show;
        this.read         = read;
        this.release_time = release_time;
        this.expired_time = expired_time;
    }

    static init(value: any) {
        let instance = new NotificationModel();
        if (value === null) return instance;
        instance.doc_id       = value.doc_id || '';
        instance.id           = value.id;
        instance.topic        = value.topic || '';
        instance.label        = value.label || '';
        instance.title        = value.title || '';
        instance.excerpt      = value.excerpt || '';
        instance.content      = value.content || '';
        instance.src          = value.src || '';
        instance.bg           = value.bg || '';
        instance.link         = value.link || '';
        instance.show         = value.show || false;
        instance.read         = value.read || false;
        instance.release_time = value.release_time || '';
        instance.expired_time = value.expired_time || '';
        return instance;
    }

    toObject() {
        return {
            id           : this.id           ,
            topic        : this.topic        ,
            label        : this.label        ,
            title        : this.title        ,
            excerpt      : this.excerpt      ,
            content      : this.content      ,
            src          : this.src          ,
            bg           : this.bg           ,
            link         : this.link         ,
            show         : this.show         ,
            read         : this.read         ,
            release_time : this.release_time ,
            expired_time : this.expired_time ,
        };
    }
}
