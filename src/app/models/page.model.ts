export class PageModel {
    id         : number  ;
    parent_id  : number  ;
    order      : number  ;
    show       : boolean ;
    title      : string  ;
    icon       : string  ;
    url        : string  ;
    children   : PageModel[]  ;

    constructor(id: number, parent_id: number, order: number, show: boolean, title: string, icon: string, url: string, children: PageModel[]) {
        this.id = id;
        this.parent_id = parent_id;
        this.order = order;
        this.show = show;
        this.title = title;
        this.icon = icon;
        this.url = url;
        this.children = children;
    }
}
