import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'verses',
        // redirectTo: 'pool',
        pathMatch: 'full'
    },

    // Alerts
    {
        path: 'alerts',
        loadChildren: () => import('./pages/alert/alert-preview/alert-preview.module').then(m => m.AlertPreviewPageModule)
    },
    // Notifications
    {
        path: 'notifications',
        loadChildren: () => import('./pages/notifications/notification-list/notification-list.module').then(m => m.NotificationListPageModule)
    },
    {
        path: 'notifications/:id',
        loadChildren: () => import('./pages/notifications/notification-detail/notification-detail.module').then(m => m.NotificationDetailPageModule)
    },
    // Verses
    {
        path: 'verses',
        loadChildren: () => import('./pages/daily-verse/daily-verse-list/daily-verse-list.module').then(m => m.DailyVerseListPageModule)
    },
    {
        path: 'verses/:id',
        loadChildren: () => import('./pages/daily-verse/daily-verse-detail/daily-verse-detail.module').then(m => m.DailyVerseDetailPageModule)
    },
    // Pool
    {
        path: 'pool',
        loadChildren: () => import('./pages/pool/pool-list/pool-list.module').then(m => m.PoolListPageModule)
    },
    {
        path: 'pool/:id',
        loadChildren: () => import('./pages/pool/pool-update/pool-update.module').then(m => m.PoolUpdatePageModule)
    },
    // API Monitor
    {
        path: 'monitors',
        loadChildren: () => import('./pages/monitor/monitor-list/monitor-list.module').then(m => m.MonitorListPageModule)
    },
    {
        path: 'monitors/:idx',
        loadChildren: () => import('./pages/monitor/monitor-detail/monitor-detail.module').then(m => m.MonitorDetailPageModule)
    },


];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
