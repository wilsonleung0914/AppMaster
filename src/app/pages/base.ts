import {Component, OnDestroy} from '@angular/core';
import { Router, NavigationEnd, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';

@Component({
    template: ''
})
export abstract class BasePage implements OnDestroy {

    private ngUnsubscribe: Subject<void> = new Subject();

    protected constructor(router: Router, route: ActivatedRoute) {
        router.events.pipe(
            takeUntil(this.ngUnsubscribe),
            filter(event => event instanceof NavigationEnd),
            filter(_ => this._isComponentActive(
                router.routerState.snapshot.root.pathFromRoot,
                route.snapshot.component
            ))
        ).subscribe(_ => this.onEnter());
    }

    private _isComponentActive(path: ActivatedRouteSnapshot[], component: any): boolean {
        let isActive = false;
        path.forEach((ss: ActivatedRouteSnapshot) => {
            if (ss.component === component) {
                isActive = true;
            } else {
                isActive = this._isComponentActive(ss.children, component);
            }
        });
        return isActive;
    }

    abstract onEnter(): void;

    public ngOnDestroy(): void {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    /* ==================================================================================================== */

    public fetchData() {}

    public refreshContent(event) {
        this.hide('content-container');
        this.show('refresher');
        this.fetchData();
        setTimeout(() => {
            this.hide('refresher');
            this.show('content-container');
            event.target.complete();
        }, 2000);
    }

    public show(id) {
        document.getElementById(id).style.display = 'block';
    }
    public shows(ids) {
        ids.forEach(item => document.getElementById(item).style.display = 'block');
    }
    public hide(id) {
        document.getElementById(id).style.display = 'none';
    }
    public hides(ids) {
        ids.forEach(item => document.getElementById(item).style.display = 'none');
    }

}
