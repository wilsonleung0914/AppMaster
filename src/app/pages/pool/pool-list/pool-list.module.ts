import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {RouterModule, Routes} from '@angular/router';
import {ComponentsModule} from '../../../components/components.module';

import {NgPipesModule} from 'ngx-pipes';
import {PoolListPage} from './pool-list.page';

const routes: Routes = [
    {
        path: '',
        component: PoolListPage,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        IonicModule,
        ComponentsModule,
        NgPipesModule,
    ],
    declarations: [PoolListPage],
})
export class PoolListPageModule {
}
