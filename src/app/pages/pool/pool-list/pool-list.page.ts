import {Component, OnDestroy, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import * as Constants from '../../../providers/constants';
import {BasePage} from '../../base';
import {GlobalVars} from '../../../providers/globalVars';
import {AlertModel} from '../../../models/alert.model';
import {MeetingModel} from '../../../models/meeting.model';
import {utf8Encode} from '@angular/compiler/src/util';

@Component({
    selector: 'pool-list.page',
    templateUrl: 'pool-list.page.html',
    styleUrls: ['./pool-list.page.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PoolListPage extends BasePage implements OnDestroy {

    timestamp   : string;
    // preview     : string;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public globalVars: GlobalVars,
    ) {
        super(router, activatedRoute);
        // this.preview = '';
    }

    ionViewWillEnter() {
    }
    ionViewWillLeave() {
    }

    async onEnter() {
        await this.fetchData();
    }

    onDestroy() {
        super.ngOnDestroy();
    }

    refreshContent(event) {
        this.show('refresher');
        this.fetchData()
            .then(() => {});
        setTimeout(() => {
            this.hide('refresher');
            event.target.complete();
        }, 2000);
    }

    async fetchData() {
        // this.globalVars.dataService.stagingAlerts$ = this.globalVars.firebaseService.getStagingAlerts();
        // this.globalVars.dataService.stagingAlerts$
        //     .subscribe(items => {
        //         console.log('items: ', items);
        //     });
    }

    /* ==================================================================================================== */

    /* Manipulate Views */

    formatItemTitle(item: AlertModel | MeetingModel) {
        if (item instanceof AlertModel) {
            return `${item.id}_${item.category}`;
        } else if (item instanceof MeetingModel) {
            return `${item.title}`;
        }
    }

    formatDate(date: string) {
        return this.globalVars.dateService.formatDate(date, Constants.DATE_FORMAT_TC);
    }

    formatBackground(bg) : any {
        if (bg.includes('#')) {
            return {'background': bg};
        } else {
            return {'background-image': 'url('+ bg +')'};
        }
    }

    updateTimestamp() {
        this.timestamp = this.globalVars.dateService.formatDateString(this.globalVars.dateService.getCurrentTime(), Constants.TIMESTAMP_FORMAT_TC, '最後更新時間：');
    }

    /* ==================================================================================================== */

    /* Operations */

    runUpdate(item: AlertModel | MeetingModel) {
        console.log('item: ', item);
        this.globalVars.firebaseService.runUpdate(item)
            .then(() => {
                item.color = `#000000`;
            });
        // if (item instanceof AlertModel) {
        //     item.position = (item.show) ? item.color : item.position;
        //     item.color = `${(item.show) ? item.position : '#000000'}`
        // } else {
        // }
    }

    updateRebrandly(item) {
        if (item instanceof MeetingModel) {
            this.globalVars.firebaseService.linkService.updateLink(item)
                .subscribe((response) => {
                    console.log('response: ', response);
                });
        }
    }

    togglePool(item: AlertModel | MeetingModel) {
        item.show = !item.show;
        this.runUpdate(item);
        // item.color = `${(item.show) ? '#420940' : '#000000'}`
    }

}
