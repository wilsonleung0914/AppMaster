import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {NgxMaskModule} from 'ngx-mask';

import {PoolUpdatePage} from './pool-update.page';


const routes: Routes = [
    {
        path: '',
        component: PoolUpdatePage,
    }
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule.forRoot(),
        RouterModule.forChild(routes),
    ],
    declarations: [PoolUpdatePage],
})
export class PoolUpdatePageModule {
}
