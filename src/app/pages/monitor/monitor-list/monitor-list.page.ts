import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GlobalVars} from '../../../providers/globalVars';
import {FirebaseService} from '../../../providers/firebaseService';
import {BasePage} from '../../base';

@Component({
    selector: 'monitor-list.page',
    templateUrl: './monitor-list.page.html',
    styleUrls: ['./monitor-list.page.scss'],
})
export class MonitorListPage extends BasePage implements OnInit, OnDestroy {

    searchQuery: string;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public globalVars: GlobalVars,
        public firebaseService: FirebaseService,
    ) {
        super(router, activatedRoute);
    }

    ionViewWillEnter() {
        this.ngOnInit();
    }

    ionViewWillLeave() {
        this.ngOnDestroy();
    }

    ngOnInit() {
        this.searchQuery = '';

    }

    ngOnDestroy(): void {
    }

    async onEnter() {
    }

    /* ==================================================================================================== */

    /* Manipulate Views */

    formatHeader(header: string) {
        return (header.includes('error')) ? `<span class="caution">${header}</span>` : header;
    }
    formatAmount(amount: number) {
        return `(${amount})`;
    }

    /* ==================================================================================================== */
    /* Operations */

    async clearOldLogs() {
        await this.globalVars.messageService.presentConfirmationAlert(
            '即將清除舊記錄',
            '確認要清除嗎？',
            '',
            () => {
                // this.globalVars.logService.getOldLogs()
                //     .subscribe(values => {
                //         this.globalVars.messageService.presentToast('Old Logs are deleted.')
                //     });
            },
        );
    }

    openDetail(logs: any, pos: number) {
        this.router.navigate(['/monitors/' + pos], {
            state: {
                key: logs.key,
                logs: logs.values,
            }
        }).then(() => {});
    }

}
