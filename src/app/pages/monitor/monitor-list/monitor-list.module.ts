import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ComponentsModule} from '../../../components/components.module';
import {NgPipesModule} from 'ngx-pipes';

import {FirebaseService} from '../../../providers/firebaseService';
import {MonitorListPage} from './monitor-list.page';

const routes: Routes = [
    {
        path: '',
        component: MonitorListPage,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        ComponentsModule,
        NgPipesModule,
    ],
    declarations: [MonitorListPage],
    providers: [
        FirebaseService,
    ]
})
export class MonitorListPageModule {
}
