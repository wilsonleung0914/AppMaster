import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ComponentsModule} from '../../../components/components.module';
import {NgPipesModule} from 'ngx-pipes';

import {MonitorDetailPage} from './monitor-detail.page';
import {LogAccordionComponent} from '../../../components/log-accordion/log-accordion.component';

const routes: Routes = [
    {
        path: '',
        component: MonitorDetailPage,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        ComponentsModule,
        NgPipesModule
    ],
    declarations: [
        MonitorDetailPage,
        LogAccordionComponent,
    ],
    providers: [
    ]
})
export class MonitorDetailPageModule {
}
