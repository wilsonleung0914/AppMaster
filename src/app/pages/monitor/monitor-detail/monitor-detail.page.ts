import {Component, OnDestroy} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';

import * as Constants from '../../../providers/constants';
import {BasePage} from '../../base';
import {GlobalVars} from '../../../providers/globalVars';
import {FirebaseService} from '../../../providers/firebaseService';
import {filter, map, tap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {ServerLogModel} from '../../../models/server-log.model';

@Component({
    selector: 'monitor-detail.page',
    templateUrl: './monitor-detail.page.html',
    styleUrls: ['./monitor-detail.page.scss'],
})
export class MonitorDetailPage extends BasePage implements OnDestroy {

    monitor_key: string;
    logCollection : ServerLogModel[];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public globalVars: GlobalVars,
        public firebaseService: FirebaseService,
        public modalController: ModalController,
    ) {
        super(router, activatedRoute);
        this.monitor_key = '';

        if (this.router.getCurrentNavigation().extras.state) {
            const state = this.router.getCurrentNavigation().extras.state;
            this.monitor_key = state.key;
            this.logCollection = state.logs;
        }
    }

    async onEnter() {
        await this.fetchData();
        await this.fetchViews();
    }

    onDestroy() {
        super.ngOnDestroy();
    }

    async fetchData() {
        // this.activatedRoute.params.subscribe(params => {
        //     this.monitor_index = params['idx'];
        //     this.logs$ = this.globalVars.dataService.logs$
        //         .pipe(
        //             // map(items => items),
        //             // filter(item => item == this.monitor_index)
        //             tap(output => console.log('api output: ', output[0])),
        //
        //         );
            // this.globalVars.dataService.get(Constants.COLLECTION.SERVER_LOGS)
            //     .then(values => {
            //         let collection = values.find(item => item.key == this.monitor_index);
            //         for (let group in collection.values) {
            //             this.logCollection = collection.values[group];
            //         }
            //     });
        // });
    }

    /* ==================================================================================================== */

    /* Manipulate Views */

    async fetchViews() {
    }

    /* ==================================================================================================== */

    /* Operations */

}
