import {Component, OnDestroy, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import * as Constants from '../../../providers/constants';
import {BasePage} from '../../base';
import {GlobalVars} from '../../../providers/globalVars';
import {AlertModel} from '../../../models/alert.model';
import {VerseModel} from '../../../models/verse.model';

@Component({
    selector: 'alert-edit.page',
    templateUrl: 'alert-edit.page.html',
    styleUrls: ['./alert-edit.page.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AlertEditPage extends BasePage implements OnDestroy {

    customPatterns: any;
    tempAlert : AlertModel;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public globalVars: GlobalVars,
    ) {
        super(router, activatedRoute);
        this.customPatterns = {
            T: {
                pattern: new RegExp(/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/gm)
            },
        };
        this.tempAlert = AlertModel.init(null);
    }

    ionViewWillEnter() {
    }
    ionViewWillLeave() {
    }

    async onEnter() {
        await this.fetchData();
    }

    onDestroy() {
        super.ngOnDestroy();
    }

    refreshContent(event) {
        this.show('refresher');
        this.fetchData()
            .then(() => {});
        setTimeout(() => {
            this.hide('refresher');
            event.target.complete();
        }, 2000);
    }

    async fetchData() {
        await this.activatedRoute.params.subscribe(params => {
            this.globalVars.getItem(Constants.COLLECTION.ALERTS)
                .then(value => {
                    let target = value.find(item => item.position === `home_${params['pos']}`);
                    this.tempAlert = Object.assign(AlertModel.init(null), target);
                });
        });
        setTimeout(() => this.configureViews(), 100);
    }

    /* ==================================================================================================== */

    /* Manipulate Views */

    formatDate(date: string) {
        return this.globalVars.dateService.formatDate(date, Constants.DATE_FORMAT_TC);
    }

    configureViews() {
        (<HTMLInputElement>document.querySelector(`ion-input[name='release_time']`)).value = this.tempAlert.release_time;
        (<HTMLInputElement>document.querySelector(`ion-input[name='expired_time']`)).value = this.tempAlert.expired_time;
        (<HTMLInputElement>document.querySelector(`ion-input[name='color']`)).value = this.tempAlert.color;
        (<HTMLInputElement>document.querySelector(`ion-textarea[name='content']`)).value   = this.tempAlert.content.replace(/\s/g, '<br/>');
    }

    /* ==================================================================================================== */

    /* Operations */

    goBack() {
        this.globalVars.navigatePage(`alert-preview`)
            .then(() => {});
    }

    validatePattern(value: any, pattern: any) {
        return new RegExp(pattern).test(value);
    }

    validateInputs(name: string) {
        let value;
        switch (name) {
            case 'release_time':
                value = (<HTMLInputElement>document.querySelector(`ion-input[name='${name}']`)).value;
                if (this.globalVars.isEmpty(value)) {
                    return false;
                } else {
                    if (!this.validatePattern(value, this.customPatterns.T.pattern)) {
                        return false;
                    } else {
                        this.tempAlert.release_time = value;
                        return true;
                    }
                }
            case 'expired_time':
                value = (<HTMLInputElement>document.querySelector(`ion-input[name='${name}']`)).value;
                if (this.globalVars.isEmpty(value)) {
                    return false;
                } else {
                    if (!this.validatePattern(value, this.customPatterns.T.pattern)) {
                        return false;
                    } else {
                        this.tempAlert.expired_time = value;
                        return true;
                    }
                }
            case 'color':
                value = (<HTMLInputElement>document.querySelector(`ion-input[name='${name}']`)).value;
                if (this.globalVars.isEmpty(value)) {
                    return false;
                } else {
                    this.tempAlert.color = value;
                    return true;
                }
            case 'content':
                value = (<HTMLInputElement>document.querySelector(`ion-textarea[name='${name}']`)).value;
                if (this.globalVars.isEmpty(value)) {
                    return false;
                } else {
                    // handle line breaks
                    this.tempAlert.content = value.replace(/\n/g, '<br/>');
                    return true;
                }
        }
    }

    validateAlert() {
        return (
            this.validateInputs('release_time') &&
            this.validateInputs('expired_time') &&
            this.validateInputs('color') &&
            this.validateInputs('content')
        );
    }

    updateAlert() {
        // this.globalVars.firebaseService.updateAlert(this.tempAlert)
        //     .then(() => this.goBack());
    }

}
