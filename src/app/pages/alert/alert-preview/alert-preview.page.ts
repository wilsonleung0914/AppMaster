import {Component, OnDestroy, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import * as Constants from '../../../providers/constants';
import {BasePage} from '../../base';
import {GlobalVars} from '../../../providers/globalVars';
import {AlertModel} from '../../../models/alert.model';
import {tap} from 'rxjs/operators';

@Component({
    selector: 'alert-preview.page',
    templateUrl: 'alert-preview.page.html',
    styleUrls: ['./alert-preview.page.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AlertPreviewPage extends BasePage implements OnDestroy {

    alerts      : AlertModel[];
    alertTop    : AlertModel;
    alertBottom : AlertModel;
    timestamp   : string;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public globalVars: GlobalVars,
    ) {
        super(router, activatedRoute);
        this.alertTop    = AlertModel.init({});
        this.alertBottom = AlertModel.init({});
    }

    ionViewWillEnter() {
    }
    ionViewWillLeave() {
    }

    async onEnter() {
        await this.fetchData();
    }

    onDestroy() {
        super.ngOnDestroy();
    }

    refreshContent(event) {
        this.show('refresher');
        this.fetchData()
            .then(() => {});
        setTimeout(() => {
            this.hide('refresher');
            event.target.complete();
        }, 2000);
    }

    async fetchData() {
        await this.globalVars.dataService.alert$
            .pipe(
                tap(values => {
                    this.alertTop    = values.find(item => item.position === 'home_top');
                    this.alertBottom = values.find(item => item.position === 'home_bottom');
                    this.updateTimestamp();
                })
            )
            .subscribe(alerts => {
            });
    }

    /* ==================================================================================================== */

    /* Manipulate Views */

    formatDate(date: string) {
        return this.globalVars.dateService.formatDate(date, Constants.DATE_FORMAT_TC);
    }

    updateTimestamp() {
        this.timestamp = this.globalVars.dateService.formatDateString(this.globalVars.dateService.getCurrentTime(), Constants.TIMESTAMP_FORMAT_TC, '最後更新時間：');
    }

    /* ==================================================================================================== */

    /* Operations */

    editAlert(position: string) {
        this.globalVars.goToPageByURL(`alert-edit`, position);
    }

}
