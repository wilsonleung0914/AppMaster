import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {RouterModule, Routes} from '@angular/router';
import {AlertPreviewPage} from './alert-preview.page';
import {ComponentsModule} from '../../../components/components.module';

const routes: Routes = [
    {
        path: '',
        component: AlertPreviewPage,
    }
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        ComponentsModule,
        RouterModule.forChild(routes),
    ],
    declarations: [AlertPreviewPage],
})
export class AlertPreviewPageModule {
}
