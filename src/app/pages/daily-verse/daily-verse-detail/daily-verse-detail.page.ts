import {Component, OnDestroy, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MenuController, ModalController} from '@ionic/angular';

import * as Constants from '../../../providers/constants';
import {BasePage} from '../../base';
import {GlobalVars} from '../../../providers/globalVars';
import {VerseModel} from '../../../models/verse.model';
import {DailyVerseFormModal} from '../daily-verse-form/daily-verse-form.modal';
import {map} from 'rxjs/operators';

@Component({
    selector: 'daily-verse-detail.page',
    templateUrl: 'daily-verse-detail.page.html',
    styleUrls: ['./daily-verse-detail.page.scss'],
    encapsulation: ViewEncapsulation.None
})
export class DailyVerseDetailPage extends BasePage implements OnDestroy {

    showingVerse: VerseModel;
    isEdited = false;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private menuCtrl: MenuController,
        private modalController : ModalController,
        public globalVars: GlobalVars,
    ) {
        super(router, activatedRoute);
        this.showingVerse = VerseModel.init({});
    }

    ionViewWillEnter() {
        this.menuCtrl.enable(false)
            .then(() => {});
    }
    ionViewWillLeave() {
        this.menuCtrl.enable(true)
            .then(() => {});
    }

    async onEnter() {
        this.globalVars.updateDate();
        await this.fetchData();
    }

    onDestroy() {
        super.ngOnDestroy();
    }

    refreshContent(event) {
        this.show('refresher');
        this.fetchData()
            .then(() => {});
        setTimeout(() => {
            this.hide('refresher');
            event.target.complete();
        }, 2000);
    }

    async fetchData() {
        await this.activatedRoute.params.subscribe(params => {
            this.globalVars.dataService
                .verse$
                .pipe(
                    map(value => value.find(item => item.id === +params['id']))
                )
                .subscribe(value => {
                    this.showingVerse = value;
                });
        });
    }

    /* ==================================================================================================== */

    /* Manipulate Views */

    formatDate(date: string) {
        return this.globalVars.dateService.formatDate(date, Constants.DATE_FORMAT_TC);
    }

    checkEditStatus() {
        return this.isEdited;
    }

    /* ==================================================================================================== */

    /* Operations */

    async openVerseFormModal() {
        const modal = await this.modalController.create({
            component: DailyVerseFormModal,
            componentProps: {
                verse: this.showingVerse,
            }
        });
        modal.onDidDismiss().then(() => {
            if (this.globalVars.currentVerse.doc_id !== undefined) {
                this.showingVerse = this.globalVars.currentVerse;
                this.isEdited = true;
            }
        });
        return await modal.present();
    }

    updateVerse() {
        this.globalVars.firebaseService.updateDailyVerse(this.globalVars.currentVerse)
            .then(() => this.globalVars.currentVerse = VerseModel.init(null));
    }
}
