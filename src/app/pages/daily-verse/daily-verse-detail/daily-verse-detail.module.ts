import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {ComponentsModule} from '../../../components/components.module';

import {RouterModule, Routes} from '@angular/router';
import {DailyVerseDetailPage} from './daily-verse-detail.page';


const routes: Routes = [
    {
        path: '',
        component: DailyVerseDetailPage,
    }
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        ComponentsModule,
        RouterModule.forChild(routes),
    ],
    declarations: [DailyVerseDetailPage],
})
export class DailyVerseDetailPageModule {
}
