import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {NgxMaskModule} from 'ngx-mask';
import {ComponentsModule} from '../../../components/components.module';

import {DailyVerseFormModal} from './daily-verse-form.modal';

const routes: Routes = [
    {
        path: '',
        component: DailyVerseFormModal,
    }
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        ComponentsModule,
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule.forRoot(),
        RouterModule.forChild(routes),
    ],
    declarations: [DailyVerseFormModal]
})
export class DailyVerseFormModalModule {
}
