import {Component, ViewChild} from '@angular/core';
import {IonSegment, MenuController, ModalController, NavParams} from '@ionic/angular';

import {FirebaseService} from '../../../providers/firebaseService';
import {GlobalVars} from '../../../providers/globalVars';
import {VerseModel} from '../../../models/verse.model';

@Component({
    selector: 'daily-verse-form.modal',
    templateUrl: './daily-verse-form.modal.html',
    styleUrls: ['./daily-verse-form.modal.scss'],
})
export class DailyVerseFormModal {

    @ViewChild(IonSegment, {static: true}) modeSegment : IonSegment;

    id             : number;
    verse          : VerseModel;
    temp_verse     : VerseModel;
    customPatterns : any;

    constructor(
        private menuCtrl        : MenuController,
        private modalController : ModalController,
        private navParams       : NavParams,
        public globalVars       : GlobalVars,
        public firebaseService  : FirebaseService
    ) {
        this.customPatterns = {
            E: {
                pattern: new RegExp(/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/gm),
                symbol: 'X'
            },
            T: {
                pattern: new RegExp(/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/gm),
            },
            C: {
                pattern: new RegExp(/\p{sc=Han}+/gu),
            }
        };
    }

    ionViewWillEnter() {
        this.menuCtrl.enable(false)
            .then(() => {});
        this.verse = this.navParams.get('verse');
        setTimeout(() => this.configureViews(), 100);
    }
    ionViewWillLeave() {
        this.menuCtrl.enable(true)
            .then(() => {});
    }

    // ngOnInit() {
        // this.verse = this.navParams.get('verse');
        // setTimeout(() => this.configureViews(), 100);
    // }

    dismissModal() {
        this.modalController.dismiss()
            .then(() => {});
    }

    configureViews() {
        this.temp_verse = Object.assign(new VerseModel(), this.verse);
        (<HTMLInputElement>document.querySelector(`ion-input[name='release_time']`)).value = this.temp_verse.release_time;
        (<HTMLInputElement>document.querySelector(`ion-input[name='expired_time']`)).value = this.temp_verse.expired_time;
        (<HTMLInputElement>document.querySelector(`ion-textarea[name='content']`)).value   = this.temp_verse.content.replace(/\s/g, '<br/>');
        (<HTMLInputElement>document.querySelector(`ion-input[name='verse']`)).value        = this.temp_verse.verse;
        document.getElementById('btn-replace').click();
    }

    changeMode(event : any) {
        this.modeSegment.value = event.target.value;
        console.log('segment value:', this.modeSegment.value);
        switch (this.modeSegment.value) {
            case 'edit':
                (<HTMLInputElement>document.querySelector(`ion-textarea[name='content']`)).value = this.temp_verse.content.replace(/\s/g, '<br/>');
                (<HTMLInputElement>document.querySelector(`ion-input[name='verse']`)).value      = this.temp_verse.verse;
                break;
            case 'replace':
                (<HTMLInputElement>document.querySelector(`ion-textarea[name='content']`)).value = '';
                (<HTMLInputElement>document.querySelector(`ion-input[name='verse']`)).value      = '';
                break;
        }
    }

    validatePattern(value : any, pattern : any) {
        return new RegExp(pattern).test(value);
    }

    validateInputs(name : string) {
        let value;
        switch (name) {
            case 'release_time':
                value = (<HTMLInputElement>document.querySelector(`ion-input[name='${name}']`)).value;
                if (this.globalVars.isEmpty(value)) {
                    return false;
                } else {
                    if (!this.validatePattern(value, this.customPatterns.T.pattern)) {
                        return false;
                    } else {
                        this.temp_verse.release_time = value;
                        return true;
                    }
                }
            case 'expired_time':
                value = (<HTMLInputElement>document.querySelector(`ion-input[name='${name}']`)).value;
                if (this.globalVars.isEmpty(value)) {
                    return false;
                } else {
                    if (!this.validatePattern(value, this.customPatterns.T.pattern)) {
                        return false;
                    } else {
                        this.temp_verse.expired_time = value;
                        return true;
                    }
                }
            case 'content':
                (<HTMLInputElement>document.querySelector(`ion-textarea[name='${name}']`)).value
                    = (<HTMLInputElement>document.querySelector(`ion-textarea[name='${name}']`)).value
                    .replace('(','（')
                    .replace(')','）')
                    .trim();
                value = (<HTMLInputElement>document.querySelector(`ion-textarea[name='${name}']`)).value
                    .trim();
                if (this.globalVars.isEmpty(value)) {
                    return false;
                } else {
                    // remove prefix
                    const content = value.replace(/\d\S+日/gm, '');
                    // let content = value.replace(/(\d.*)\n\n/, '').trim();

                    // console.log('content: ', content);
                    // handle line break
                    this.temp_verse.content = content
                        .split('\n\n')[0]
                        .replace(/\n+/g, '<br/>')
                        .replace(/^<br\s*\/?>|<br\s*\/?>$/g,'');

                    this.temp_verse.content = this.temp_verse.content.split('<br/><br/>')[0];

                    // handle verse location
                    const matchArray = content.match(/（.*?）/gm);
                    if (matchArray) {
                        (<HTMLInputElement>document.querySelector(`ion-input[name='verse']`)).value = '';
                        this.temp_verse.verse = matchArray[0];
                        this.temp_verse.content = this.temp_verse.content.replace(this.temp_verse.verse, '').trim();
                        //
                        // console.log('content: ', this.temp_verse.content);
                        // console.log('verse: ', this.temp_verse.verse);
                    }
                    return true;
                }
            case 'verse':
                (<HTMLInputElement>document.querySelector(`ion-input[name='${name}']`)).value
                    = (<HTMLInputElement>document.querySelector(`ion-input[name='${name}']`)).value
                    .replace('(','（')
                    .replace(')','）')
                    .trim();
                value = (<HTMLInputElement>document.querySelector(`ion-input[name='${name}']`)).value;
                if (this.globalVars.isEmpty(value)) {
                    return false;
                } else {
                    this.temp_verse.verse = value;
                    return true;
                }
        }
    }

    validateRequired() {
        return !(
            this.validateInputs('release_time') &&
            this.validateInputs('expired_time') &&
            this.validateInputs('content') &&
            (this.validateInputs('verse') || this.temp_verse.verse)
        );
    }

    updateVerse() {
        // prepare verse content
        this.temp_verse.content = this.temp_verse.content.split('<br/><br/>')[0];

        // prepare verse object
        this.globalVars.currentVerse = Object.assign(new VerseModel(), this.temp_verse);
        this.dismissModal();

        // this.globalVars.firebaseService.updateDailyVerse(this.temp_verse)
        //     .then(() => this.dismissModal());
    }

}
