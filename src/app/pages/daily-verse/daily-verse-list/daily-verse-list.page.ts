import {AfterViewInit, Component, OnDestroy, ViewEncapsulation} from '@angular/core';

import {GlobalVars} from '../../../providers/globalVars';
import {VerseModel} from '../../../models/verse.model';
import {BasePage} from '../../base';
import {ActivatedRoute, Router} from '@angular/router';
import {take} from 'rxjs/operators';


@Component({
    selector: 'daily-verse-list.page',
    templateUrl: 'daily-verse-list.page.html',
    styleUrls: ['./daily-verse-list.page.scss'],
    encapsulation: ViewEncapsulation.None
})
export class DailyVerseListPage extends BasePage implements AfterViewInit, OnDestroy {

    showingVerses  : VerseModel[] ;
    fallbackVerses : VerseModel[] ;
    todayVerse     : VerseModel   ;
    showAll        : boolean      ;

    constructor(
        private router : Router,
        private activatedRoute : ActivatedRoute,
        public globalVars : GlobalVars,
    ) {
        super(router, activatedRoute);
        this.showingVerses = this.fallbackVerses = [];
        this.todayVerse = VerseModel.init({});
        this.showAll    = false;
        this.globalVars.dataService.verse$
            .pipe(take(1))
            .subscribe(items => {
                this.fallbackVerses = items;
                this.showingVerses  = items.filter(item => item.date <= this.globalVars.dateService.getDateAfter(2));
                this.todayVerse     = items.find(item => item.date === this.globalVars.dateService.getTodayDate());
            });
    }

    ngAfterViewInit() {
        setTimeout(() => this.selectTodayVerse(), 3000);
    }

    async onEnter() {
        setTimeout(() => this.configureView(), 1000);
    }

    onDestroy() {
        super.ngOnDestroy();
    }

    /* ==================================================================================================== */

    /* Manipulate Views */

    formatItemTitle(date : string) {
        switch(date) {
            case this.globalVars.dateService.getTodayDate():
                return `${date}<span class='day today'>(今日)</span>`;
            case this.globalVars.dateService.getDateAfter(1):
                return `${date}<span class='day tomorrow'>(明日)</span>`;
            default:
                return date;
        }
    }

    async configureView() {
        this.selectTodayVerse();
    }

    selectTodayVerse() {
        const verseElement = document.querySelector(`#verse_${this.todayVerse.id}`);
        if (verseElement !== null) {
            verseElement.scrollIntoView();
        }
    }

    toggleList() {
        this.showAll = !this.showAll;
        this.showingVerses = (this.showAll) ? this.fallbackVerses : this.fallbackVerses.filter(item => item.date <= this.globalVars.dateService.getDateAfter(2));
    }

    /* ==================================================================================================== */

    /* Operations */


}
