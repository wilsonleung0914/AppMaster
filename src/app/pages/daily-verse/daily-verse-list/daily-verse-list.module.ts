import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgPipesModule} from 'ngx-pipes';

import {DailyVerseListPage} from './daily-verse-list.page';

const routes: Routes = [
    {
        path: '',
        component: DailyVerseListPage,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        NgPipesModule,
    ],
    declarations: [
        DailyVerseListPage,
    ]
})
export class DailyVerseListPageModule {
}
