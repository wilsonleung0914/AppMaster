import {Component, OnInit, ViewChild} from '@angular/core';
import {IonSegment, MenuController, ModalController} from '@ionic/angular';

import {FirebaseService} from '../../../providers/firebaseService';
import {NotificationModel} from '../../../models/notification.model';
import {GlobalVars} from '../../../providers/globalVars';

@Component({
    selector: 'notification-form.modal',
    templateUrl: './notification-form.modal.html',
    styleUrls: ['./notification-form.modal.scss'],
})
export class NotificationFormModal implements OnInit {

    // @ViewChild(IonSegment, {static: true}) visibilitySegment: IonSegment;

    id             : number;
    customPatterns : any;
    notification   : NotificationModel;
    page_title     : string;
    btn_text       : string;

    currentView : string;

    constructor(
        private menuCtrl: MenuController,
        private modalController : ModalController,
        public globalVars       : GlobalVars,
        public firebaseService  : FirebaseService
    ) {
        this.customPatterns = {
            'E': {
                pattern: new RegExp(/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/gm),
                symbol: 'X'
            },
            'T': {
                pattern: new RegExp(/^\d{2}-\d{2} \d{2}:\d{2}$/gm),
            }
        };
        this.currentView = 'content';
    }

    ionViewWillEnter() {
        this.menuCtrl.enable(false)
            .then(() => {});
        this.ngOnInit();
    }
    ionViewWillLeave() {
        this.menuCtrl.enable(true)
            .then(() => {});
    }

    ngOnInit() {
        this.configureViews();
    }

    dismissModal() {
        this.modalController.dismiss()
            .then(() => {});
    }
    /* ==================================================================================================== */

    /* Manipulate Views */

    configureViews() {
        if (this.id === undefined) {
            this.notification = NotificationModel.init({});
            this.notification.id = this.globalVars.notificationCollection[0].id + 1;
        } else {
            this.notification = Object.assign(new NotificationModel(), this.globalVars.notificationCollection.find(item => item.id === this.id));
            (<HTMLInputElement>document.querySelector(`ion-input[name='title']`)).value        = this.notification.title;
            (<HTMLInputElement>document.querySelector(`ion-textarea[name='content']`)).value   = this.notification.content;
            (<HTMLInputElement>document.querySelector(`ion-input[name='release_time']`)).value = this.notification.release_time.substring(5, this.notification.release_time.length - 3);
            (<HTMLInputElement>document.querySelector(`ion-input[name='expired_time']`)).value = this.notification.expired_time.substring(5, this.notification.release_time.length - 3);
            (<HTMLInputElement>document.querySelector(`ion-input[name='bg']`)).value           = this.notification.bg;
            // this.visibilitySegment.value = String(this.notification.show);
        }
        this.page_title = (this.id === undefined) ? '新增通知' : '修改通知';
        this.btn_text   = (this.id === undefined) ? '新增' : '修改';
        return this.id === undefined;
    }

    segmentChanged(ev: any) {
        this.currentView = ev.target.value;
    }

    validatePattern(value: any, pattern: any) {
        return new RegExp(pattern).test(value);
    }

    validateInputs(name: string) {
        let value;
        switch (name) {
            case 'title':
                value = (<HTMLInputElement>document.querySelector(`ion-input[name='${name}']`)).value;
                if (this.globalVars.isEmpty(value)) {
                    return false;
                } else {
                    this.notification.title = value;
                    return true;
                }
            case 'content':
                value = (<HTMLInputElement>document.querySelector(`ion-textarea[name='${name}']`)).value;
                if (this.globalVars.isEmpty(value)) {
                    return false;
                } else {
                    // handle line break
                    this.notification.content = value.replace(/\n+/g, '<br/><br/>');
                    // handle hyperlink
                    this.notification.content = this.globalVars.linkify(this.notification.content);
                    return true;
                }
            case 'release_time':
                value = (<HTMLInputElement>document.querySelector(`ion-input[name='${name}']`)).value;
                if (this.globalVars.isEmpty(value)) {
                    return false;
                } else {
                    if (!this.validatePattern(value, this.customPatterns.T.pattern)) {
                        return false;
                    } else {
                        this.notification.release_time = `2020-${value}:00`;
                        return true;
                    }
                }
            case 'expired_time':
                value = (<HTMLInputElement>document.querySelector(`ion-input[name='${name}']`)).value;
                if (this.globalVars.isEmpty(value)) {
                    return false;
                } else {
                    if (!this.validatePattern(value, this.customPatterns.T.pattern)) {
                        return false;
                    } else {
                        this.notification.expired_time = `2020-${value}:00`;
                        return true;
                    }
                }
        }
    }

    validateRequired() {
        return !(
            this.validateInputs('title') &&
            this.validateInputs('content') &&
            this.validateInputs('release_time') &&
            this.validateInputs('expired_time')
        );
    }

    clearContent() {
        (<HTMLInputElement>document.querySelector(`ion-textarea[name='content']`)).value = '';
    }

    configBackgroundURL(src: string) {
        let bg = '';
        // No BG
        if (this.globalVars.isEmpty(src)) {
            bg = `../../../../assets/images/bg_sky.jpg`;
        }
        // Web BG
        else if (src.startsWith('http://') || src.startsWith('https://')) {
            bg = src + `?nocache=${this.globalVars.dateService.getCurrentTimestamp()}`;
        }
        else if (src === 'web') {
            bg = `http://placeimg.com/1080/1620/any?nocache=${this.globalVars.dateService.getCurrentTimestamp()}`;
        }
        // Local BG
        else {
            bg = src;
        }
        return bg;
    }

    changeVisibility(event: any) {
        this.notification.show = JSON.parse(event.target.value);
    }

    importNotification() {
        this.notification.bg = this.configBackgroundURL((<HTMLInputElement>document.querySelector('ion-input[name=\'bg\']')).value);
        this.globalVars.currentNotification = this.notification;
        this.dismissModal();
    }

}
