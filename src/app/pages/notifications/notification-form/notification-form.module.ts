import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask';
import {RouterModule, Routes} from '@angular/router';
import {ComponentsModule} from '../../../components/components.module';


import {NotificationFormModal} from './notification-form.modal';



const routes: Routes = [
    {
        path: '',
        component: NotificationFormModal,
    }
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        ComponentsModule,
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule.forRoot(),
        RouterModule.forChild(routes),
    ],
    declarations: [NotificationFormModal]
})
export class NotificationFormModalModule {
}
