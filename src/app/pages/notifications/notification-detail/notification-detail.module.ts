import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';

import {ComponentsModule} from '../../../components/components.module';
import {NotificationDetailPage} from './notification-detail.page';

const routes: Routes = [
    {
        path: '',
        component: NotificationDetailPage,
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ComponentsModule
    ],
    declarations: [NotificationDetailPage],
    providers: [
    ]
})
export class NotificationDetailPageModule {
}
