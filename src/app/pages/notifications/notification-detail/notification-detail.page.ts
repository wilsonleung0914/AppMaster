import {Component, OnDestroy} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';

import * as Constants from '../../../providers/constants';
import {BasePage} from '../../base';
import {GlobalVars} from '../../../providers/globalVars';
import {FirebaseService} from '../../../providers/firebaseService';
import {NotificationModel} from '../../../models/notification.model';
import {NotificationFormModal} from '../notification-form/notification-form.modal';
import {filter, map, tap} from 'rxjs/operators';

@Component({
    selector: 'notification-detail.page',
    templateUrl: './notification-detail.page.html',
    styleUrls: ['./notification-detail.page.scss'],
})
export class NotificationDetailPage extends BasePage implements OnDestroy {

    notification: NotificationModel;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public globalVars: GlobalVars,
        public firebaseService: FirebaseService,
        public modalController: ModalController,
    ) {
        super(router, activatedRoute);
        this.notification = NotificationModel.init(null);
    }

    async onEnter() {
        await this.fetchData();
        await this.fetchViews();
    }

    onDestroy() {
        super.ngOnDestroy();
    }

    async fetchData() {
        this.activatedRoute.params.subscribe(params => {
            this.globalVars.dataService
                .notification$
                .pipe(
                    map(value => value.find(item => item.id === +params['id']))
                )
                .subscribe(value => {
                    this.notification = value;
                });
        });
    }

    /* ==================================================================================================== */

    /* Manipulate Views */

    async fetchViews() {
        // handle background
        // let element = document.getElementById('main-content') as HTMLElement;
        // element.style.setProperty('background', this.configBackgroundURL(this.notification.bg));

        // handle img
        setTimeout(() => {
            let elements = document.querySelectorAll('img');
            elements.forEach(item => {
                // (<HTMLElement>item).addEventListener('click', () => this.enlargeImage(item.src, ''));
            });
        }, 50);
    }

    configBackgroundURL(src: string) {
        let url = '';
        // No BG
        if (this.globalVars.isEmpty(src)) {
            url = `../../../../assets/images/bg_whoats.jpg`;
        }
        // Web BG
        else if (src.startsWith('http://') || src.startsWith('https://')) {
            url = src + `?nocache=${this.globalVars.dateService.getCurrentTimestamp()}`;
        }
        // Local BG
        else {
            url = src;
        }
        return `linear-gradient(rgba(255,255,255,.5), rgba(255,255,255,.5)), url('${url}') no-repeat center center / cover`;
    }

    /* ==================================================================================================== */

    /* Operations */

    refreshContent(event) {
        this.show('refresher');
        this.fetchData()
            .then(() => {});
        setTimeout(() => {
            this.hide('refresher');
            event.target.complete();
        }, 2000);
    }

    async openNotificationFormModal() {
        const modal = await this.modalController.create({
            component: NotificationFormModal,
            componentProps: {
                id: this.notification.id,
            }
        });
        modal.onDidDismiss().then(() => {
            if (this.globalVars.currentNotification.doc_id !== undefined) {
                this.notification = this.globalVars.currentNotification;
            }
        });
        await modal.present();
    }

    updateNotification() {
        this.globalVars.firebaseService.updateNotification(this.globalVars.currentNotification)
            .then(() => this.globalVars.currentNotification = NotificationModel.init(null));
    }

}
