import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {ComponentsModule} from '../../../components/components.module';
import {NgPipesModule} from 'ngx-pipes';

import {FirebaseService} from '../../../providers/firebaseService';
import {NotificationListPage} from './notification-list.page';

const routes: Routes = [
    {
        path: '',
        component: NotificationListPage,
    }
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        ComponentsModule,
        FormsModule,
        NgPipesModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
    ],
    declarations: [NotificationListPage],
    providers: [
        FirebaseService,
    ]
})
export class NotificationListPageModule {
}
