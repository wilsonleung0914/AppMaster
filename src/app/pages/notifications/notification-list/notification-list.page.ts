import {Component, HostBinding, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ModalController} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';

import {merge, Observable, ReplaySubject, Subscription} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';
import {GlobalVars} from '../../../providers/globalVars';
import {FirebaseService} from '../../../providers/firebaseService';
import {NotificationModel} from '../../../models/notification.model';
import {NotificationFormModal} from '../notification-form/notification-form.modal';
import {BasePage} from '../../base';

@Component({
    selector: 'notification-list.page',
    templateUrl: './notification-list.page.html',
    styleUrls: ['./notification-list.page.scss'],
})
export class NotificationListPage extends BasePage implements OnInit, OnDestroy {
// export class NotificationManagementPage extends BasePage implements OnDestroy {

    searchQuery: string;

    // Use Typescript intersection types to enable docorating the Array of firebase models with a shell model
    // (ref: https://www.typescriptlang.org/docs/handbook/advanced-types.html#intersection-types)
    // items: Array<NotificationModel> & ShellModel;
    items: NotificationModel[];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public globalVars: GlobalVars,
        public firebaseService: FirebaseService,
        public modalController: ModalController,
    ) {
        super(router, activatedRoute);
    }

    ionViewWillEnter() {
        // this.items = Object.assign(this.globalVars.notificationCollection, {isShell: true});
        this.ngOnInit();
    }

    ionViewWillLeave() {
        this.ngOnDestroy();
    }

    ngOnInit() {
        this.searchQuery = '';

    }

    ngOnDestroy(): void {
    }

    async onEnter() {
    }

    async openNotificationFormModal() {
        const modal = await this.modalController.create({
            component: NotificationFormModal,
        });
        await modal.present();
    }
}
